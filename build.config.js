module.exports = {
    "base": "theme/src",
    "entries": [
        "theme/src/index.js",
        "theme/src/scss/srsly.scss"
    ],
    "dist": 'theme/dist',
    "copyImage": {
        "from": "theme/src/img",
        "to": "theme/dist/img"
    },
    "providers": {
      "$": "jquery",
      "jQuery": "jquery"
    },
    "favicon": {
        "title": "SRSLY Web Project",
        "path": "theme/src/favicons/favicon_master.png",
        "background": "red",
        "icons": {
            "android": true,
            "appleIcon": true,
            "appleStartup": true,
            "coast": false,
            "favicons": true,
            "firefox": true,
            "opengraph": false,
            "twitter": false,
            "yandex": false,
            "windows": false
        }
    }
}
