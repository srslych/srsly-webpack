#!/usr/bin/env node

const cli = require('commander');
const shell = require('shelljs');
const packageJson = require('./../package.json');
const versionCheck = require('./../lib/version-check.js');

cli
    .version(packageJson.version, '-v, --version')

cli
    .command('build')
    .description('Build assets for production using webpack')
    .action(() => shell.exec('NODE_ENV=production yarn run webpack --config ./node_modules/@srsly/webpack/config/webpack.prod.js'))

cli
    .command('watch')
    .description('Watch files and autoreload for development')
    .action(() => shell.exec('yarn run webpack-dev-server --https --config ./node_modules/@srsly/webpack/config/webpack.dev.js'))

cli
    .command('versioncheck')
    .description('Check if your installed version of @srsly/webpack matches with the latest version on NPM')
    .action(() => versionCheck.checkForUpdate())

cli.parse(process.argv);
