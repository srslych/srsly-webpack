const checkLatestVersion = require('latest-version');
const chalk = require('chalk');
const packageJson = require('./../package.json');

module.exports = {
    checkForUpdate: function() {
        const currentVersion = packageJson.version;
        const latestVersion = checkLatestVersion('@srsly/webpack');

        Promise.all([currentVersion, latestVersion]).then(values => {
            if(values[0] === values[1]) {
                console.log(
                    chalk`
                        {green ✅ INSTALLED VERSION MATCHED LATEST VERSION ON NPM ✅}
                    `
                )
                return true;
            } else {
                console.log(
                    chalk`
                        {red ===============================================================================}
                        {red 💥 VERSION MISMATCH. MAY THERE IS A NEWER VERSION OF {underline @SRSLY/WEBPACK} AVAILABLE 💥}
                        {red                        ${values[0]} ≠≠≠ ${values[1]}}
                        {red ===============================================================================}
                    `
                )
                process.exit();
            }
        }).catch(error => {
            console.error(error);
            process.exit();
        })
    }
}
