# Prerequisite
1. Make sure you have installed `nvm` (https://github.com/creationix/nvm) to manage your node version
2. If you have installed nvm, type `nvm use` to use the defined node version from .nvmrc. If your project does not have
a `.nvmrc` file, create one!
3. make sure you have yarn installed on your machine
4. run `yarn` from your project root to install your project dependencies

# Install @srsly/webpack package
Run `yarn add --dev @srsly/webpack` to install the webpack config as dev dependency.

# Setup

## Files
1. Create `index.js` in `theme/src`
2. Create `postcss.config.js`, `tsconfig.json`, `build.config.json` and `.stylelintrc` on your project root


### Existing Project
1. Migrate installed dependencies from `theme/package.json` to the `package.json` no your root dir.
2. Delete `theme/package.json`, `theme/package-lock.json`, `theme/gulpfile.js`, `theme/yarn.lock`, `theme/.sass-cache`, `theme/node_modules`, `src/config/*` and `js/vendor`

## postcss.config.js
```
module.exports = {
	plugins: {
		'autoprefixer': {}
	}
}
```

## build.config.json
```
module.exports = {
    "entries": [
        "theme/src/index.js",
        "theme/src/scss/srsly.scss"
    ],
    "dist": 'theme/dist',
    "copyImage": {
        "from": "theme/src/img",
        "to": "theme/dist/img"
    },
    "favicon": {
        "title": "SRSLY Web Project",
        "background": "red",
        "icons": {
            "android": true,
            "appleIcon": true,
            "appleStartup": true,
            "coast": false,
            "favicons": true,
            "firefox": true,
            "opengraph": false,
            "twitter": false,
            "yandex": false,
            "windows": false
        }
    }
}
```

## tsconfig.json
Create a `tsconfig.json` file in your project's root with the following content to extend from
our typescript base config.
```
{
    "extends": "./node_modules/@srsly/webpack/tsconfig.json",
}
```

## .stylelintrc
Create `.stylelintrc` and extend from our base config.
```
{
    "extends": "./node_modules/@srsly/webpack/.stylelintrc"
}
```

### srsly.scss
Load needed bootstrap modules from from node_modules.
```
@import "~bootstrap/scss/functions";
@import "~bootstrap/scss/variables";
@import "~bootstrap/scss/mixins";

... more modules
```

## Add scripts to your package.json
Add the following scripts to your package.json
```
  "scripts": {
    "dev": "yarn run srsly watch,
    "build": "yarn run srsly build",
    "lint:scss": "stylelint \"theme/**/*.scss\" --syntax scss",
    "lint:scss:fix": "stylelint \"theme/**/*.scss\" --fix --syntax scss",
    "prettier:check": "prettier 'theme/src/**/*.ts' --list-different",
    "prettier:fix": "prettier 'theme/src/**/*.ts' --write",
    "test": "yarn run lint:scss && yarn run prettier:check"
  },
```

# Commands

## SRSLY CLI
- Use `yarn run srsly --help` so see all commands.

## Installing New Dependencies
- Use `yarn add <package>` to install a package from npm used as production dependency
- Use `yarn add --dev <package>` to install a package from npm used as dev dependency

## Development Mode
type `yarn run dev` to start a webpack server with HMR and source maps.

## Production Mode
type `yarn run build` to build your assets for production. you can inspect your dependencies with `yarn run build:analyze`

## Linting & Testing
Check your project with `yarn run test`

### Stylelint for SCSS (https://stylelint.io/)
- Lint files: `yarn run lint:scss`
- Lint files and fix (if possible): `yarn run lint:scss:fix`

### Prettier for Typescript (https://prettier.io/)
- Check files `yarn run prettier:check`
- Fix files `yarn run prettier:fix`

# Publishing new versions
use `npm publish` to publish a new version of this config to npm. Please follow semver https://semver.org.

# Todos

## Srsly CLI
- Bootstrap Command to create `tsconfig.json`, `build.config.js`, etc..

## Webpack
- Refactoring Codebase

## Version Checker
- Advanced features like autoupdate version, show installed and remote version, etc.