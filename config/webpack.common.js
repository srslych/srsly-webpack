const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const Webpack = require('webpack');
const config = require(`${process.cwd()}/build.config.js`);
const versionCheck = require('./../lib/version-check.js');

versionCheck.checkForUpdate();

module.exports = { 
    entry: config.entries.map(i => `${process.cwd()}/${i}`),
    output: {
        path: `${process.cwd()}/${config.dist}`,
        filename: "[name].bundle.js"
    },
    resolve: {
        extensions: [ '.jsx', '.js' ]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/,
                loader: 'imports-loader?jQuery=jquery'
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true,
                        },
                    },
                ],
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin([config.dist, `${process.cwd()}/theme/dist/*.css`, `${process.cwd()}/theme/dist/*.js`], {beforeEmit: false}),
        new FaviconsWebpackPlugin({
            logo: path.resolve(__dirname, `${process.cwd()}/${config.favicon.path}`),
            prefix: 'favicons/',
            background: config.favicon.background,
            title: config.favicon.title,
            icons: config.favicon.icons
        }),
        new Webpack.ProvidePlugin(config.providers),
        new CopyWebpackPlugin([
            {
                from: `${process.cwd()}/${config.copyImage.from}`,
                to: `${process.cwd()}/${config.copyImage.to}`
            },
            {
                from: path.resolve(__dirname, `${process.cwd()}/${config.base}/index.html`),
                to: path.resolve(__dirname, `${process.cwd()}/${config.base}/index.html`)
            },
        ]),
    ],
};
