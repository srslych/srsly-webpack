const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const Webpack = require('webpack');
const path = require('path');
const config = require(`${process.cwd()}/build.config.js`);

module.exports = merge(common, {
    devtool: "inline-source-map",
    devServer: {
        contentBase: path.resolve(__dirname, `${process.cwd()}/${config.dist}`),
        hot: true
    },
    module:  {
        rules: [
            {
                test: /.scss$/,
                loader: 'style-loader!css-loader?sourceMap!sass-loader?sourceMap&sourceComments'
            },
        ]
    },
    plugins: [
        new Webpack.NamedModulesPlugin(),
        new Webpack.HotModuleReplacementPlugin()
    ]
});
